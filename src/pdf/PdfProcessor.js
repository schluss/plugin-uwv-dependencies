const json = require('./template.json');
const attributeList = json.attributes;
const tableList = json.tables;
const contentList = json.content_lists;
const uniqueList = json.unique;
const customList = json.custom;

var filePath;
/**
 * Reading PDF File Content Processor.
 * 
 */
const PdfProcessor = {
    readFile: (fileLocation) => {
        return new Promise((resolve, reject) => {
            getContentFromFile(fileLocation).then((data) => {
                resolve(getTemplateData(data));
            });
        });
    }
};

/**
 * Read PDF Content
 * @param {string} fileEntry 
 * @returns {string} pdfContent
 */
function getContentFromFile(fileEntry) {
    return new Promise((resolve, reject) => {
        console.log("Reading the PDF ");
        filePath = fileEntry;
        cordova.plugins.GoPdf.read(
            fileEntry,
            (e) => {
                resolve(e.message);
            },
            (e) => {
                console.log("Extracting Error " + e.message);

                reject(e.message);
            }
        );
    });
}

/**
 * Process Content with Template
 * @param {String} result 
 */
async function getTemplateData(result) {
    console.log('Get Template Data - ' + result);

    let response=[];
    let resultList = {};
    let returnList = {};

    //Process Unique Attributes
    let getUniqueAttributes = new Promise((resolve, reject) => {
        let datalist = null;

        uniqueList.forEach((data) => {
            if (data.visible == "true") {               
                datalist = processUniqueAttributes(data, result);
                
                resultList[data.attribute] = datalist;
            }
        });
        resolve(resultList);
    });

    await getUniqueAttributes;

    //Process Tables Attributes
    let getTables = new Promise((resolve, reject) => {
        tableList.forEach((table) => {
            if (table.visible == "true") {               
                let datalist = processTables(table, result);
                resultList[table.attribute] = datalist;
            }
        });
        
        resolve(resultList);
    });

    await getTables;
    
    //Process Content List Attributes
    let getContentList = new Promise((resolve, reject) => {
        contentList.forEach((content) => {
            if (content.visible == "true") {            
                let datalist = processContentList(content, result);
                resultList[content.attribute] = datalist;
            }
        });

        resolve(resultList);
    });

    await getContentList; 

    //Process Custom Attributes
    let getCustomAttributes = new Promise((resolve, reject) => {
        customList.forEach((data) => {
            if (data.visible == "true") {               
                processCustomAttributes(data, resultList).then((resultData) => {
                    resultList[data.attribute] = resultData;
                });
            }
        });
        resolve(resultList);
    });

    await getCustomAttributes;

    let getFileBase64 = await processFile();

    //prepare final Json List
    attributeList.forEach((attribute) => {
        if(attribute.type =='file'){
            response.push({"name": attribute.name, "value":getFileBase64});
        }else{
            response.push({"name": attribute.name, "value":resultList[attribute.name]});
        }
    });

    if (response.length ==  attributeList.length) {
        returnList["payload"] =  response;
        return new Promise(function (resolve, reject) {
            console.log("PDF Response : " + JSON.stringify(returnList));
            resolve(JSON.stringify(returnList));
        });
    }
    return null;
}

/**
 * Process Unique Attributes Dynamically using Template
 * @param {*} uniQueData 
 * @param {*} result 
 */
function processUniqueAttributes(uniQueData, result) {

    let startWord = uniQueData.content_start;  
    let endWord = uniQueData.content_end;
    let contentFlag = uniQueData.content_flag;
    let regexResultList = regexExecuter(null,startWord, endWord, contentFlag, result);

    const content = regexResultList[0];

    let dataStart = uniQueData.data_start;
    let dataEnd = uniQueData.data_end;
    let dataFlag = uniQueData.data_flag;

    let uniqueDataList = regexExecuter(null,dataStart, dataEnd, dataFlag, content);
    let data = uniqueDataList[0];

    data = data.replace(dataStart, "");

    if(uniQueData.regex != ""){
       let regexString = uniQueData.regex;
       let customRegexResultList = regexExecuter(regexString,null,null,dataFlag, data);
       data = customRegexResultList[0];
    }

    if (data.split(" ")[uniQueData.index] != undefined && data.split(" ")[uniQueData.index] != "") {
        return processString(data.split(" ")[uniQueData.index]);
    }

    return null;
}

/**
 * Process Table Attributes Dynamically using Template
 * @param {*} uniQueData 
 * @param {*} result 
 */
function processTables(tableData, result) {

    let startWord = tableData.content_start;  
    let endWord = tableData.content_end;
    let contentFlag = tableData.content_flag;
    let datalist = [];

    let regexContentResultList = regexExecuter(null,startWord, endWord, contentFlag, result);

    regexContentResultList.forEach((content) => {
        let tableTitlesArray = tableData.table_header;
        let tableendString = tableData.table_end;
        let tableFlag = tableData.table_flag;

        let regexDataList = regexExecuter(null,tableTitlesArray, tableendString, tableFlag, content);

        regexDataList.forEach((data) => {
            let dataArray = {};

            data = data.replace(tableTitlesArray, "");
            data.split("\n").filter((data) => {
                tableData.attributes.forEach((attribute) => {
                    if (data.split(" ")[attribute.index] != undefined && data != "" && data.split(" ")[attribute.index] != ""){
                        dataArray[attribute.attribute] = processString(data.split(" ")[attribute.index]);
                    }
                });

                if(Object.keys(dataArray).length == tableData.attributes.length){
                    datalist.push(dataArray);
                }
            });
        });
    });
    return  datalist;
}

/**
 * Process Content List Attributes Dynamically using Template
 * @param {*} uniQueData 
 * @param {*} result 
 */
function processContentList(contentList, result) {

    let startWord = contentList.content_start;  
    let endWord = contentList.content_end;
    let contentListFlag = contentList.content_flag;

    let regexContentResultList = regexExecuter(null,startWord, endWord, contentListFlag, result);

    let datalist = [];

    regexContentResultList.forEach((match, groupIndex) => {
        let contentStart = contentList.list_start;
        let contentEnd = contentList.list_end;
        let contentFlag = contentList.list_flag;

        let regexDataList = regexExecuter(null,contentStart, contentEnd, contentFlag, match);

        regexDataList.forEach((match2) => {
            let test = match2.replace(contentList.list_start, "");

            const [firstline, otherlines] = match2.split(/(?<=^[^\n]+)\n/);

            let dataArray = {};

            dataArray["jaar"] = processString(firstline.replace(contentList.list_start, ""));
            dataArray["omschrijving"] = processString(otherlines.split("Totaal:")[0]);
            dataArray["totaal"] = otherlines.split("Totaal:")[1] == undefined ? "" : processString(test.split("Totaal:")[1]);
            datalist.push(dataArray);
        });
    });
    return datalist;
}

/**
 * Process Custom Attributes Dynamically using Template
 * @param {*} customData 
 * @param {*} resultList
 */
function processCustomAttributes(customData, resultList) {
    
    let functionName = customData.functionName;
    let param = customData.param;   

    return new Promise((resolve, reject) => {
        let fu = functionName+"(param,resultList)";
        resolve(eval(fu));
    })
}


function getAge(params,resultList){

    letBdayFieldName = params.bday;
    letBdayFieldValue = resultList[letBdayFieldName];
    var today = new Date();
    var parts = letBdayFieldValue.split("-")
    var birthDate = new Date(parts[2], parts[1] - 1, parts[0])

    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }

    return age;
}

function getCurrentEmploymentStatus(params,resultList){

    letWageDataFieldName = params.wageData;
    letWageDataFieldValue = resultList[letWageDataFieldName];
    
    let employmentStatus = false ;

    letWageDataFieldValue.slice(0,3).forEach((data) => {
        employmentStatus = false ;
        if (data.aantal > 0) {    
            employmentStatus = true;           
        }
    });

    return employmentStatus;
}

/*
Will build the regex string depending on the parameter
*/
function regexBuilder(regexString,startWord, endWord, flags) {
    let regex = "";

    if(regexString==null){
        regex += startWord!="" ?  "(?="+startWord+"\\s*).+" :".*";
        regex += endWord  !="" ?  "?(?=\\s*"+endWord+")" :"";
    }else{
        regex = regexString;
    }

    return new RegExp(regex, flags);
} 

/**
 * Will Execute the Regex files and return the result list
 * @param {*} startWord 
 * @param {*} endWord 
 * @param {*} contentFlag 
 * @param {*} stringValue 
 */
function regexExecuter(regexString,startWord, endWord, contentFlag, stringValue) {
    let valueList = [];
    let m;

    let regex = regexBuilder(regexString,startWord, endWord, contentFlag);

    while ((m = regex.exec(stringValue)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }

        m.forEach((match, groupIndex) => {
            valueList.push(match);
        });
    }

    return valueList;
}

function processString(dataString) {
    return dataString.replace("\n","").trim();
}

function processFile() {
    return new Promise((resolve, reject) => {
        getFileContentAsBase64(filePath,(e) => {
            resolve(e);
        });
    });
}

function getFileContentAsBase64(path,callback){
    window.resolveLocalFileSystemURL(path, gotFile, fail);
            
    function fail(e) {
          alert('Cannot found requested file');
    }

    function gotFile(fileEntry) {
        fileEntry.file(function(file) {
            var reader = new FileReader();
            reader.onloadend = function(e) {
                var content = this.result;
                callback(content);
            };
            // The most important point, use the readAsDatURL Method from the file plugin
            reader.readAsDataURL(file);
        });
    }
}

module.exports = PdfProcessor;