function createFile (dirEntry, fileName, data, isAppend) {

	return new Promise(function(resolve, reject){
		// Creates a new file or returns the file if it already exists.
		dirEntry.getFile(fileName, {create: true, exclusive: false}, function(fileEntry) {
			writeFile(fileEntry, data, isAppend).then(function(fileEntry){
				resolve(fileEntry);
			});
		}, onErrorCreateFile);

		function onErrorCreateFile(err){
			console.log(err);
			reject(err);
		}	
	});
}

function writeFile(fileEntry, dataObj) {
	
	return new Promise(function(resolve, reject){
	
		// Create a FileWriter object for our FileEntry (log.txt).
		fileEntry.createWriter(function (fileWriter) {

			fileWriter.onwriteend = function() {
				console.log("Successful file write..." + JSON.stringify(fileEntry));
				//readFile(fileEntry);
				resolve(fileEntry);
			};

			fileWriter.onerror = function (e) {
				console.log("Failed file write: " + JSON.stringify(e));
				reject();
			};

			// If data object is not passed in,
			// create a new Blob instead.
			if (!dataObj) {
				dataObj = new Blob(['No Data Found'], { type: 'text/plain' });
			}

			fileWriter.write(dataObj);
		});
	
	})
}


module.exports = {
    /**
     *  Getting Download file URL 
     *      Params:
     *          - url: URL need to be opened
     *          - target: target type ( _blank / _self / _system )
     *          - Options: addional options provided in Inappbrowser
     *          - expectFileTypes: Expected File types/Extension array 
	 */
	downloadUrl: (url, target, options, expectDownloadLink) => {
		return new Promise((resolve, reject) => {
			let ref = window.open(url, target, options);
	
	
			// matching url must be done in loadstart event to make this work with uwv website
			// the beforeload event interrupts a redirect which results in a not working login
			ref.addEventListener('loadstart', (event) => { 
				console.log("InappBrowser loadstart triggered, url: " + event.url);
				ref.show();	
				
				console.log("Search for match: " + expectDownloadLink);

				if (event.url.search(expectDownloadLink) != -1){
					console.log('match found, url: ' + expectDownloadLink);
					ref.close();
					resolve(event.url);
				}				
			});

			
            ref.addEventListener('loadstop', (event) => {
				console.log("InappBrowser loadstop event triggered, url: " + event.url );
				
				console.log('Inject some helper code into the page: css and js');
				ref.insertCSS({code: "#dvb-pdfoverzicht .btn { background:#fa4a69 !important; padding:20px !important; min-width:90% !important;}"});
				ref.executeScript({code: "document.getElementById('dvb-pdfoverzicht').scrollIntoView();"});
				
				console.log("Search for match within loadstop event: " + expectDownloadLink);

				if (event.url.search(expectDownloadLink) != -1){
					console.log('match found, url: ' + expectDownloadLink);
					ref.close();
					resolve(event.url);
				}					
			});
			
			ref.addEventListener('loaderror', (event) => {
				console.log("InappBrowser load stop Error triggered" + event.message + event.code);
			});
		
			ref.addEventListener('exit', (event) => {
				console.log("InappBrowser exit event triggered");
				
				ref.close();
				ref = undefined;
			});
		});
	},

	/**
     *  Download files 
     *      Params:
     *          - downloadUrl: URL need to be Download
     *          - downloadLocation: Download location
     *      return 
     *          - Downloaded Location
     */
	download: (downloadUrl, fileName)=> {
		
		return new Promise((resolve, reject) => {
			req = new XMLHttpRequest();
			req.open('GET', downloadUrl, true);

			req.responseType = "arraybuffer";
			req.onload = function() {
				if (this.status === 200) {
					var blob = new Blob([req.response], {type: "application/pdf"});

					window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
						window.resolveLocalFileSystemURL(window.cordova.file.externalDataDirectory, function (dirEntry) {
						console.log(window.cordova.file.externalDataDirectory);
							createFile(dirEntry, fileName, blob).then(function(fileEntry){
								var downloadedLocation = window.cordova.file.externalDataDirectory + fileEntry.name;
								resolve(downloadedLocation);										
							});
						}, onErrorLoadFs);
					}, onErrorLoadFs);
					
					function onErrorLoadFs(err){
						console.log(err);
						reject(err);
					}
				}
			}
			req.send();
		});
	},
    
    /**
     *  Processing file
     *      Params:
     *          - providerId : URL need to be Download
     *          - downloadedFileLocation : Downloaded file location
     *      return 
     *          - transaction info.
     */
    processFile: (providerId, downloadedFileLocation) => {
        return new Promise((resolve, reject) => {
            import(/* webpackMode: "eager" */`./providers.json`).then((json) => {
                let providersList = json.providers;
                let provider = providersList[providerId].providerId;

                if (provider === providerId) {
					import(/* webpackMode: "eager" */`./config`).then((config) => {
                        config.readFile(downloadedFileLocation).then((e) => {
                            resolve(e);
                        }).catch((error) => {
							console.log(error.message);
							
                            reject(error.message);
                        });
                    }).catch((error) => {
						console.log(error);
						
                        reject(error.message);
                    });
                } else {
                    reject("Providers Not found :" + providerId);
                }
            }).catch((error) => {
				console.log(error);
				
                reject(error.message);
            });
        });    
	},
	
	deleteDownloadedFile: (downloadedFileLocation) => {
		const filePath = downloadedFileLocation.substr(0, downloadedFileLocation.lastIndexOf('/'));
		window.resolveLocalFileSystemURL(filePath, (dir) => {
		  const fileName = downloadedFileLocation.substr(downloadedFileLocation.lastIndexOf('/') + 1);
		  dir.getFile(fileName, { create: false }, (fileEntry) => {
			fileEntry.remove(() => {
				console.log('file removed!');
			}, (error) => reject('error occurred: ' + error.code),
			  () => console.log('file does not exist'));
		  }, (error) => console.log(JSON.stringify(error)));
		}, (error) => console.log(JSON.stringify(error)));
	}
}