/**
 *  Functionality of UWV Plugin.
 * 
 */
const providersJson = require('./providers.json');
const util = require('./util');


module.exports = {
    init: (context) => {
        if (! context.request) {
            console.log("generate sample request object");

			context.request = {
				id: '018eedd1-d21b-448b-91f0-da5db6a03006', // this is the requestID (format UUIDv4).
				name: 'de Volksbank',
				pubkey: '-----BEGIN PGP PUBLIC KEY BLOCK----- ... -----END PGP PUBLIC KEY BLOCK-----', // the users public key.
				settingsuri: 'https://url-to-volksbank.config.json',	// if needed, the plugin can find additional details about what is requested by downloading the config.json combined with the 'scope' parameter.
				scope: 'hypotheekaanvraag'
			};
        }
        

		let providersList = providersJson.providers;
		
		let url = providersList['uwv'].loginUrl;
		let target = "_blank"; 
		let options = "location=no,hidden=yes,zoom=no";
		
		// import related plugin json file from selected company folder location:
		import(/* webpackMode: "eager" */`./plugin.json`).then((json) => {
			let pluginList = json.plugins;
			let expectFileType = Object.keys(pluginList);  // Registered expected file types.
			
			if (expectFileType !== undefined) {
				let expectDownloadLink = pluginList[expectFileType].downloadpage;      // Setting Plug-in source.

				util.downloadUrl(url, target, options, expectDownloadLink).then(async (downloadUrl) => {
		
					// show processing screen
					context.processingView();

					// let fileName = downloadUrl.split('/').pop();  
					let fileName = tools.uuidv4()+"."+expectFileType;
					
					return util.download(downloadUrl, fileName);
					
				//when download process is finished:
				}).then((downloadedLocation) => {   
					util.processFile('uwv', downloadedLocation).then((arr) => {   // pass downloaded file in to the parser module
						context.store(arr, context.request.id).then(() =>{
							util.deleteDownloadedFile(downloadedLocation);
							// show processing done screen
							context.processingCompleted();
						});
					}).catch((error) => {
						context.processingFailedView(error.message);

						console.log(error.message);
					});							

				});
			} else {
				console.log(expectFileType);
				context.processingFailedView("Unsuported file format");
				alert("Unsuported file format");
			}
		});
   
    }
};